import React, { Component } from 'react';
import axios from "axios";
import "./ResetPassword.scss";
import "./ChangePassword.scss";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import RegistrationLogo from "../../assets/images/logo.png";
import Button from '@material-ui/core/Button';
import InputBase from '@material-ui/core/InputBase';
import RegistrationLogoBar from "../../assets/icons/LoginAndRegistration_icons/Icon-metro-menu.svg";
import RegistrationButtonArrow from "../../assets/icons/LoginAndRegistration_icons/share1.svg";
import HeartIcon from "../../assets/icons/LoginAndRegistration_icons/Icon awesome-heart.svg";
import { baseUrl } from "../../constants";
import { Link } from 'react-router-dom';
class Changepassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            oldPassword: '',
            confirmPassword: '',
            response: '',
            status: ''
        }
    }
    submitHandlerR = async (e) => {
        const id = localStorage.getItem('id');
        e.preventDefault();
        //if(this.state.password==this.state.confirmPassword){
        let requestBodyR = {
            query: `
                  mutation ChangePassword(
                      $password: String!,$oldPassword:String!,$confirmPassword:String!,$id:Int!
                    ) {
                        changePassword(
                            password: $password,oldPassword:$oldPassword,confirmPassword:$confirmPassword,id:$id
                    ) {
                        id
                    }
                  }
                `,
            variables: {
                password: this.state.password,
                oldPassword: this.state.oldPassword,
                confirmPassword: this.state.confirmPassword,
                id: Number(id)
            }
        };
        let resData = await axios({
            method: 'post',
            url: baseUrl.server,
            data: requestBodyR,
        }).then(res => {
            //alert(JSON.stringify(res));
            return res
            //window.location.href = '/login';
        }).catch(err => {
            return err
            //window.location.href = '/login';
        });

        // }else{
        //     alert("Password and Confirm Password Doesn't Match");
        // }
        if (resData.data.data.changePassword == null && resData.data.errors[0].message == 'Enter Valid Old Password!') {
            this.setState({ response: "Enter Valid Old Password!", status: "danger" });
            //window.location.href = '/';
            //alert('1');
        } else if (resData.data.data.changePassword == null && resData.data.errors[0].message == 'User doesnt exists!') {
            this.setState({ response: "The new password and confirmation password does not match", status: "danger" });
            //window.location.href = '/';
        } else {
            //alert('11');
            this.setState({ response: "Password Has Been Successfully Updated", status: "success" });
            window.location.href = '/ticketlisting';
        }

    };
    render() {
        const divStyle = {
            color: 'red',

        };
        const divStyleone = {
            color: 'green',
        };
        return (
            <div className="user-page">
                <div className="user-header-logo">
                    <AppBar position="static">
                        <Toolbar variant="dense">
                            <img src={RegistrationLogo} className="user-logo"></img>
                            <img src={RegistrationLogoBar}></img>
                        </Toolbar>
                    </AppBar>
                </div>
                <div className="resetpassword-container">
                    {
                        this.state.status == 'danger' ? (this.state.status == 'danger' ? <div className="alert alert-success alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>{this.state.response}</strong>
                        </div>
                            : <div className="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong>{this.state.response}</strong>
                            </div>

                        ) : null
                    }
                    <div className="resetpassword-container-text">
                        <p className="resetpassword-container-text-one">CHANGE YOUR PASSWORD</p>
                        <p className="resetpassword-container-text-two">Set a new password for abc@xyz.com</p>

                        {/* {this.state.status == 'danger'?   <h1 style={divStyle}>{this.state.response}</h1>
                : <h1 style={divStyleone}>{this.state.response}</h1>  } */}
                    </div>
                </div>
                <div className="resetpassword-field-one">
                    <InputBase
                        placeholder="Old Password"
                        name="oldPassword"
                        type='password'
                        onChange={e => this.setState({ oldPassword: e.target.value })}
                        value={this.state.oldPassword}
                        className="registration-input"
                        inputProps={{ 'aria-label': 'search' }}
                    />
                </div>

                <div className="resetpassword-field-one">
                    <InputBase
                        placeholder="New Password"
                        name="password"
                        type='password'
                        onChange={e => this.setState({ password: e.target.value })}
                        value={this.state.password}
                        className="registration-input"
                        inputProps={{ 'aria-label': 'search' }}
                    />
                </div>

                <div className="resetpassword-field-two">
                    <InputBase
                        placeholder="Confirm Password"
                        name="confirmPassword"
                        type='password'
                        onChange={e => this.setState({ confirmPassword: e.target.value })}
                        value={this.state.confirmPassword}
                        className="registration-input"
                        inputProps={{ 'aria-label': 'search' }}
                    />
                </div>



                <div className="change-password-field-two d-flex">
                <Button variant="outlined" color="primary" className="" onClick={this.submitHandlerR}>
                Change Password
                </Button>
                    <div className="recover-text d-flex">
                        <span><img src={RegistrationButtonArrow}></img></span>
                        <p>Click here to <br/>recover password</p>
                    </div>
                 
                </div>

                {/* <div className="bottom-text">
                    <p>Back to Login</p>
                </div> */}
                <div className="change-bottom-text">
                    <nav><Link to="/">Back to Login</Link></nav>
                </div>

        <footer className="change-password-container d-flex">
         <div className="change-password-left-container">
            <p>Made with <span><img src={HeartIcon}></img></span> in Chicago</p>
        </div>
            <div className="change-password-right-container justify-flex-end">
                <ul className="change-password-list">
                    <nav><Link to="/privacy"><li>Privacy Policy</li></Link></nav>
                    <nav><Link to="/termsOfUse"><li>Team of logo</li></Link></nav>
                    <nav><Link to="/help"><li>Helps</li></Link></nav>
                </ul>
            </div>
        </footer>
                
            </div>
        )
    }
}

export default Changepassword;
