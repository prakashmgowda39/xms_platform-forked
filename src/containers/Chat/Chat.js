import React from 'react';
import './Chat.scss';

import SearchImg from '../../assets/icons/chat/feather-search.svg';
import FvImg from '../../assets/icons/chat/material-favorite-border.svg';
import ChatImg from '../../assets/icons/chat/metro-attachment.svg';
import ProfileImg from '../../assets/icons/chat/metro-profile.svg';
import KeyboardImg from '../../assets/icons/chat/material-keyboard.svg';
import GiftImg from '../../assets/icons/chat/feather-gift.svg';
import EyeImg from '../../assets/icons/chat/awesome-eye-slash.svg';
import ClearImg from '../../assets/icons/chat/material-clear-all.svg';
import CurvyImg from '../../assets/icons/15-10-2019/Path 184.svg';
import Profile from "../../assets/images/profile.png";
import ExitImg from "../../assets/icons/chat/material-exit-to-app.svg";
import DeleteImg from "../../assets/icons/chat/material-delete-sweep.svg";
import AwesomeLayer from  "../../assets/icons/chat/Icon awesome-layer-group.svg";
import InviteIcon from "../../assets/icons/chat/Icon feather-plus.svg";
import ChatMoreEllipsis from "../../assets/icons/chat/awesome-ellipsis-h.svg";
import TextareaAttach from "../../assets/icons/chat/Icon metro-attachment-2.svg";
import TextareaSmileIcon from "../../assets/icons/chat/Icon material-tag-faces.svg";

import More1 from "../../assets/icons/chat/more-1.svg";
import More2 from "../../assets/icons/chat/more-2.svg";
import More3 from "../../assets/icons/chat/more-3.svg";
import More4 from "../../assets/icons/chat/more-4.svg";
import More5 from "../../assets/icons/chat/more-5.svg";
import More6 from "../../assets/icons/chat/more-6.svg";
import More7 from "../../assets/icons/chat/more-7.svg";
import More8 from "../../assets/icons/chat/more-8.svg";
import BottomLeft from "../../assets/icons/chat/bottom-left.svg";
import TopRight from "../../assets/icons/chat/top-right.svg";
import TopLeft from "../../assets/icons/chat/top-left.svg";
import MainLayout from '../MainLayout/MainLayout';
import Header from '../Header/Header';
import gql from 'graphql-tag';
import axios from 'axios';
import io from "socket.io-client";
import { baseUrl } from '../../constants'

const getAllCompanyUserMessages = gql`
    query getAllCompanyUserMessages($id: Int!, $user: String!) {
        getAllCompanyUserMessages(id: $id, user: $user) {
            id
            firstName
            lastName,
            lastLogin
        }
    }
`

const getUsersList = (client, id, callback) => {
    const userId = localStorage.getItem("id");
    client
        .query({query: getAllCompanyUserMessages, variables: { id: id, user: userId.toString() }})
        .then(res => {
            callback(res);
        })
}

const queryMsg = gql`
    query getUserMessages($fromUserId: String!, $toUserId: String!) {
        getUserMessages(fromUserId: $fromUserId, toUserId: $toUserId) {
            _id
            text
            fromUserId
            toUserId
            active
        }
    }
`

const getMessages = (client, fromUserId, toUserId, callback) => {
    client
      .query({query: queryMsg, variables: { toUserId: toUserId,  fromUserId: fromUserId}})
        .then(res => {
            callback(res);
        })
}

class Chat extends React.Component {
  constructor(props)  {
    super(props);

    this.state = {
      loginUserId: '',
      loginUserFName: '',
      loginUserLName: '',
      chatWithFName: '',
      chatWithLName: '',
      toUserId: '',
      activeUserToChat: null,
      toUserName: '',
      textMessage: '',
      usersList: [],
      messages: [],


      currentSideTab : "chats",
      filteringOption : "Recent Chat",
      people : [{
        name : "Isabella",
        designation : "developer",
        lastActive : "12.23 AM"
      },{
        name : "Pavanraj",
        designation : "developer",
        lastActive : "12.23 AM"
      },{
        name : "Kiran",
        designation : "Tester",
        lastActive : "12.23 AM"
      },{
        name : "Sugatha",
        designation : "Designer",
        lastActive : "12.23 AM"
      },{
        name : "Anusha",
        designation : "developer",
        lastActive : "12.23 AM"
      },{
        name : "Anusha",
        designation : "developer",
        lastActive : "12.23 AM"
      },{
        name : "Anusha",
        designation : "developer",
        lastActive : "12.23 AM"
      },{
        name : "Anusha",
        designation : "developer",
        lastActive : "12.23 AM"
      },{
        name : "Anusha",
        designation : "developer",
        lastActive : "12.23 AM"
      },{
        name : "Anusha",
        designation : "developer",
        lastActive : "12.23 AM"
      }],
      groups : [{
        name : "Marketing team",
        designation : "asdasdasd asdasd",
        lastActive : "12.23 AM"
      },{
        name : "Design team",
        designation : "asdasdasdasdasd",
        lastActive : "12.23 AM"
      },{
        name : "CodeMatrix Dev team",
        designation : "Tesasdasdasdter",
        lastActive : "12.23 AM"
      },{
        name : "tech Gigs",
        designation : "asdasdasd",
        lastActive : "12.23 AM"
      },{
        name : "Design team",
        designation : "asdasdasdasdasd",
        lastActive : "12.23 AM"
      },{
        name : "CodeMatrix Dev team",
        designation : "Tesasdasdasdter",
        lastActive : "12.23 AM"
      },{
        name : "tech Gigs",
        designation : "asdasdasd",
        lastActive : "12.23 AM"
      },{
        name : "Design team",
        designation : "asdasdasdasdasd",
        lastActive : "12.23 AM"
      }]
    }
  }

  componentDidMount() {
    this.didMountHandler()
  }

  didMountHandler = async ()=> {
    let client = this.props.client
    
    let loginUserDetails = JSON.parse(new Object(localStorage.getItem("loginUserDetails")))
    if(loginUserDetails) {
      await this.setState({
        loginUserId: loginUserDetails.userId,
        companyId: loginUserDetails.companyId,
        loginUserFName: loginUserDetails.firstName,
        loginUserLName: loginUserDetails.lastName,
      });
    }

    getUsersList(client, this.state.companyId,  response=> {
      if(response.data)
        this.setState({
          usersList: response.data.getAllCompanyUserMessages,
          activeUserToChat : response.data.getAllCompanyUserMessages[0]
        });
        this.getUserMsgs(this.state.usersList[0]);
    })
    this.initSocket(parseInt(this.state.loginUserId))
    document.addEventListener('keypress', this.sendMessageHandler)
  }

  initSocket = async (loginUserId)=> {
    const socket = await io.connect(baseUrl.socket, {
        query: { id: loginUserId }
    });
    localStorage.setItem('socketId', socket.id);
    // setTimeout(() => {
    //     if(socket.id) {
    //         localStorage.setItem('socketId', socket.id);
    //     } else {
    //       setTimeout(() => {
    //         if(socket.id) {
    //             localStorage.setItem('socketId', socket.id);
    //         } else {
    //           setTimeout(() => {
    //             localStorage.setItem('socketId', socket.id);
    //           }, 1000);
    //         }
    //       }, 500);
    //     }
    // }, 500);

    //Events
    socket.on('message', (data) => {
        if(this.state.toUserId == data.message.fromUserId)
          this.setState({ messages: [...this.state.messages, data.message] })
    });
  }

  onChange =event=> {
    this.setState({ textMessage: event.target.value })
  }
  getUserMsgs =(user)=> {
    let client = this.props.client
    getMessages(client, this.state.loginUserId.toString(), user.id.toString(), response => {
      this.setState({
        messages: response.data.getUserMessages,
        toUserId:  user.id,
        chatWithFName: user.firstName,
        chatWithLName: user.lastName,
        activeUserToChat : user
      })
    })
  }

  changeCurrentTab = (tab) => {
    this.setState({
      currentSideTab : tab
    });
  }

  changeFilteringOption = (event) => {
    this.setState({
      filteringOption : event.target.value
    });
  }


  sendMessageHandler = async (event)=>{
    let toUserId = this.state.toUserId;
    if(event.keyCode === 13) {
      if(!this.state.textMessage) {
        alert('Please type something...')
      } else if(!toUserId) {
        alert('Select user to send message')
      } else {
        let requestBody = {
          query: `
            mutation sendMessage(
                  $fromUserId: String!,
                  $toUserId: String!
                  $text: String!,
                  $socketId: String!,
              ) {
                  sendMessage(
                      fromUserId: $fromUserId,
                      toUserId: $toUserId,
                      text: $text,
                      socketId: $socketId
              ) {
                  _id
                  fromUserId
                  toUserId
                  text
                  active
              }
            }
          `,

          variables: {
              fromUserId: this.state.loginUserId.toString(),
              toUserId: toUserId.toString(),
              text: this.state.textMessage,
              socketId: localStorage.getItem('socketId')
          }

        };
        await axios({
            method: 'post',
            url: baseUrl.server,
            data: requestBody,
            headers: {
                'Content-type': 'application/json'
            }
        }).then(res => {
            this.setState({
                messages: [...this.state.messages, res.data.data.sendMessage],
                textMessage: ''
            })
        }).catch(err => console.log('err', err));
      }
    }
  }


  secondSidebar=()=> {
    return (
      <>
          <div id="sideHeader">

            <h3>Chat</h3>
            <div id="userProfile">
              <img src={Profile} alt="" />
              <div>
                <label>Nancy Pang</label>
                <label>Tester</label>
              </div>
              <span className="userStatus active"></span>
            </div>

            <div id="searchBox">
              <input type="text" name="searchChat" placeholder="Chats,people & group" />
              <img src={SearchImg} alt="" />
            </div>

            <div id="chatTabsGrid">
                <div id="chatTab"
                className={this.state.currentSideTab=="chats" ? "active" : ""}
                onClick={()=>this.changeCurrentTab('chats')}>
                  <span></span>
                  <span>Chats</span>
                </div>
                <div id="contactTab"
                className={this.state.currentSideTab=="contacts" ? "active" : ""}
                onClick={()=>this.changeCurrentTab('contacts')}>
                  <span></span>
                  <span>Contacts</span>
                </div>
                <div id="groupTab"
                className={this.state.currentSideTab=="groups" ? "active" : ""}
                onClick={()=>this.changeCurrentTab('groups')}>
                  <span></span>
                  <span>Groups</span>
                </div>
            </div>

            <div id="chatFiltering">
              <label>{ this.state.filteringOption }</label>
              <label><span>+ ADD</span>
                <div id="addOptionsDD">
                  <img src={TopLeft}/>
                  <label><img src={AwesomeLayer} alt="" />Create Group</label>
                  <label><img src={InviteIcon} alt="" />Invite New</label>
                </div>
              </label>
            </div>
          </div>

          <div id="sideBottom">
              <div className={this.state.currentSideTab=="chats" ? "displayBlock" : "displayNone"} >
                  {
                    this.state.usersList ? this.state.usersList.map(person => (
                        <div className="userProfileTag" onClick={()=> this.getUserMsgs(person)}
                        style={{ 'background-color' : this.state.activeUserToChat.id==person.id ? 'rgba(78, 88, 169, 0.2)' : 'inherit' }} >
                          <img src={Profile} alt="" />
                          <div>
                            <label> <span> {person.firstName} </span> <span>{person.lastActive}</span> </label>
                            <label>{person.designation}</label>
                          </div>
                          <span className="userStatus active"></span>
                        </div>
                    )) : null
                  }
              </div>
              <div className={this.state.currentSideTab=="contacts" ? "displayBlock" : "displayNone"} >
                  {
                    this.state.people.map(person => (
                        <div className="userProfileTag">
                          <img src={Profile} alt="" />
                          <div>
                            <label> <span> {person.name} </span> <span>{person.lastActive}</span> </label>
                            <label>{person.designation}</label>
                          </div>
                          <span className="userStatus active"></span>
                        </div>
                    ))
                  }
              </div>
              <div className={this.state.currentSideTab=="groups" ? "displayBlock" : "displayNone"} >
                  <div class="grpHdrs">FAVORITES</div>

                  {
                    <div className="userProfileGroupsTag">
                      <img src={Profile} alt="" />
                      <div>
                        <label> <span> {this.state.groups[0].name} </span> <span>{this.state.groups[0].lastActive}</span> </label>
                        <label>{this.state.groups[0].designation}</label>
                      </div>
                      <span className="userStatus active"></span>
                    </div>
                  }

                  <div class="grpHdrs">CHATS</div>
                  {
                    this.state.groups.map(group => (
                        <div className="userProfilechatsTag">
                          <img src={Profile} alt="" />
                          <div>
                            <label> <span> {group.name} </span> <span>{group.lastActive}</span> </label>
                            <label>{group.designation}</label>
                          </div>
                          <span className="userStatus active"></span>
                        </div>
                    ))
                  }
              </div>
          </div>
      </>
    )
  }

  chatBodySection=()=> {
    let { loginUserId, toUserId, chatWithFName, chatWithLName, messages, loginUserFName, textMessage, loginUserLName  } = this.state;
    return (
      <>
        <Header />
        <div className="sub-header-section">
          {
            toUserId ? (
              <div className="left">
                <label className="m-0 chatEntity">{ chatWithFName +' '+ chatWithLName }</label>
                <label className="m-0 chatEntityDescr"><span className="userStatus active"></span>Online</label>
              </div>
            ) : null
          }

          <div className="right">
            <span id="search-i"></span>
            <span id="heart-i"></span>
            <span id="attach-i"></span>
            <span id="addu-i"></span>
            <span id="moreoptns-i">
                <div id="moreoptnsDropdown">
                    <img src={TopRight}/>
                    <label className="m-0">
                      <span className="m-0"><img
                      src={FvImg} alt="" />Add to favorite</span>
                      <span className="m-0">shift+f</span>
                    </label>
                    <label className="m-0">
                      <span className="m-0"><img src={ProfileImg} alt="" />View profile</span>
                      <span className="m-0"></span>
                    </label>
                    <label className="m-0">
                      <span className="m-0"><img src={ChatImg} alt="" />Attachments</span>
                      <span className="m-0"></span>
                    </label>
                    <label className="m-0">
                      <span className="m-0"><img src={GiftImg} alt="" />What's new</span>
                      <span className="m-0 userStatus inactive">2</span>
                    </label>
                    <label className="m-0">
                      <span className="m-0"><img src={KeyboardImg} alt="" />Shortcuts</span>
                      <span className="m-0">ctrl+/</span>
                    </label>
                    <label className="m-0">
                      <span className="m-0"><img src={EyeImg} alt="" />Hide Chat</span>
                      <span className="m-0"></span>
                    </label>
                    <label className="m-0">
                      <span className="m-0"><img src={ClearImg} alt="" />Clear Chat</span>
                      <span className="m-0"></span>
                    </label>
                    <label className="m-0" style={{ display : this.state.currentSideTab=="groups" ? 'block' : 'none' }}>
                      <span className="m-0"><img src={ExitImg} alt="" />Exit Group</span>
                      <span className="m-0"></span>
                    </label>
                    <label className="m-0" style={{ display : this.state.currentSideTab=="groups" ? 'block' : 'none' }}>
                      <span className="m-0"><img src={DeleteImg} alt="" />Delete Group</span>
                      <span className="m-0"></span>
                    </label>
                </div>
            </span>
          </div>
        </div>

        <div className="chat-area-section">
            <div >
            {
              messages.map(message => (
                <div className={`chat-area-body d-flex ${loginUserId == message.fromUserId ? 'row-reverse' : '' }` }>
                  <div className="chat-area-profile">
                    <img src={Profile} alt=""></img>
                      <label>{ loginUserId == message.fromUserId ? loginUserFName +' '+ loginUserLName : chatWithFName +' '+ chatWithLName}</label>
                  </div>

                  <div className="chat-area-message">
                    {message.text}
                  </div>
                </div>
              ))
            }
            </div>
            <div className="text-area-section">
              <div>
                <span className="more-icon d-flex align-items-center justify-content-center">
                  <img src={ChatMoreEllipsis} alt="ChatMoreEllipsis"></img>
                  <div id="moreOptions">
                    <img src={BottomLeft}/>
                    <div className="block block-1">
                      <img src={More1} />
                      <span>Create<br/>Ticket</span>
                    </div>
                    <div className="block block-2">
                      <img src={More2} />
                      <span>check ticket<br/>status</span>
                    </div>
                    <div className="block block-3">
                      <img src={More3} />
                      <span>update<br/>Ticket</span>
                    </div>
                    <div className="block block-4">
                      <img src={More4} />
                      <span>Create<br/>task</span>
                    </div>

                    <div className="block block-5">
                      <img src={More5} />
                      <span>task<br/>status</span>
                    </div>
                    <div className="block block-6">
                      <img src={More6} />
                      <span>send<br/>sms</span>
                    </div>
                    <div className="block block-7">
                      <img src={More7} />
                      <span>Create<br/>habbit</span>
                    </div>
                    <div className="block block-8">
                      <img src={More8} />
                      <span>apply<br/>macro</span>
                    </div>


                  </div>
                </span>
                <div>
                  <img className="text-area-smile" src={TextareaSmileIcon} alt="TextareaAttach"></img>
                  <textarea value={textMessage} onChange={event => this.setState({ textMessage: event.target.value  })} placeholder='Type your message here'/>
                  <img className="text-area-attach" src={TextareaAttach} alt="TextareaAttach"></img>
                </div>
              </div>
            </div>
        </div>
      </>
    )
  }

  render() {
    return (
      <MainLayout secondSidebar={this.secondSidebar()} bodySection={this.chatBodySection()} />
    )
  }
}

export default Chat
